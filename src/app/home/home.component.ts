import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable} from "rxjs/Observable";
import 'rxjs/Rx';
import {Observer} from "rxjs/Observer";
import {Subscription} from "rxjs/Subscription";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {

  numbersObsSubscription: Subscription;
  customeObsSubscription: Subscription;

  constructor() { }

  ngOnInit() {
    const myNumbers = Observable.interval(1000).map( //maps data to new observable
      (data: number) => {
        return data * 2;
      });
    this.numbersObsSubscription = myNumbers.subscribe(
      (number: Number) => {
        console.log(number);
      });


    //create() takes function as argument that holds asynchronous code
    const myObservable =  Observable.create((observer: Observer<string>) => { //emits type string
        setTimeout(() => {
          observer.next('first package') //pushes next data package
        });
      setTimeout(() => {
        observer.next('second package'); //pushes next data package
      }, 2000);
      setTimeout(() => {
        observer.next('third package')
      }, 4000);
      setTimeout(() => {
        observer.error('this does not work')
      }, 5000);
      setTimeout(() => {
        observer.complete()
      }, 7000);
      });

    this.customeObsSubscription = myObservable.subscribe(
      (data:string) => {
          console.log(data);},
      (error:string) => {
        console.log(error);},
      () => {console.log('completed');}
      );
  }

  ngOnDestroy() {
    this.numbersObsSubscription.unsubscribe();
    this.customeObsSubscription.unsubscribe();
  }
}
